open Types
open List
open Printf
open Printf

module Console = struct
	let rec format = function
		| `Result (GameResult (id, date, game, scores)) ->
			let rec print_team = function
				| Player name::[] -> name ^ " "
				| Player name::tail -> name ^ ", " ^ print_team tail
				| [] -> "" in
			let print_points p = string_of_int p in
			let print_winner = function
				| Winner true -> " Winner!"
				| Winner false -> "" in
			let rec print_scores = function
				| Score (team, points, winner)::[] ->
					  print_team team ^ print_points points ^ print_winner winner
				| Score (team, points, winner)::tail ->
					  print_team team ^ print_points points ^ print_winner winner ^ " -- " ^ print_scores tail
				| [] -> "" in
			sprintf "[%d] %s -> %s: %s\n" id date game (print_scores scores)
		| `Results list -> list |> iter (fun r -> print_string (format (`Result r))); ""
		| `Player_summaries list -> list |> iter (fun (Player p, w, l) -> print_endline (sprintf "%s: win->%d lose->%d" p w l)); ""
		| `Message_int (label, num) -> sprintf "%s: %d" label num
		| `Message (label, msg) -> sprintf "%s: %s" label msg
		| `Error (label, msg) -> sprintf "%s: %s" label msg
		| `List_of_strings l ->
			let rec aux = function
				| head::tail -> head ^ "\n" ^ (aux tail)
				| [] -> "" in
			aux l
		| `Game_counts list -> list |> iter (fun (g, c) -> print_endline (sprintf "%s: %d" g c)); ""
		| `Player_counts list -> list |> iter (fun (g, c) -> print_endline (sprintf "%s: %d" g c)); ""
end

module JSON = struct
	let yojson_ok json = Yojson.Basic.to_string ~std:true (`Assoc [ "status", `String "ok"; "data", json ])
	let yojson_err err = Yojson.Basic.to_string ~std:true (`Assoc [ "status", `String "error"; "msg", err ])

	let result_to_json (GameResult (id, date, game, scores)) =
		let json_team (Player name) = `String name in
		let json_scores (Score (team, points, Winner winner)) = `Assoc [
				"players", `List (map json_team team);
				"points", `Int points;
				"winner", `Bool winner
			] in
		`Assoc [
			"id", `Int id;
			"date", `String date;
			"game", `String game;
			"scores", `List (map json_scores scores)
		]
	let rec format = function
		| `Result result -> yojson_ok (result_to_json result)
		| `Results list -> yojson_ok (`List (map result_to_json list))
		| `Player_summaries list -> yojson_ok (`List (map (fun (Player p, w, l) -> `Assoc [ "player", `String p; "wins", `Int w; "loses", `Int l ]) list))
		| `Message_int (label, num) -> yojson_ok (`Assoc [ label, `Int num ])
		| `Message (label, msg) -> yojson_ok (`Assoc [ "msg", `String (sprintf "%s: %s" label msg) ])
		| `Error (label, msg) -> yojson_err (`String (sprintf "%s: %s" label msg))
		| `List_of_strings l -> yojson_ok (`List (map (fun s -> `String s) l))
		| `Game_counts l -> yojson_ok (`List (map (fun (g, c) -> `Assoc [ "game", `String g; "count", `Int c ]) l))
		| `Player_counts l -> yojson_ok (`List (map (fun (g, c) -> `Assoc [ "player", `String g; "count", `Int c ]) l))
end

let rec scores_file = function
	| `Result (GameResult (id, date, game, scores)) ->
		let rec print_team = function
			| Player name::[] -> name ^ ""
			| Player name::tail -> name ^ "," ^ print_team tail
			| [] -> "" in
		let print_points p = sprintf "(%d)" p in
		let print_winner = function
			| Winner true -> "*"
			| Winner false -> "" in
		let rec print_scores = function
			| Score (team, points, winner)::[] ->
				  print_team team ^ print_points points ^ print_winner winner
			| Score (team, points, winner)::tail ->
				  print_team team ^ print_points points ^ print_winner winner ^ ";" ^ print_scores tail
			| [] -> "" in
		sprintf "%d:%s:%s:%s\n" id date game (print_scores scores)
