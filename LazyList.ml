module Z = struct
	(* lazy list *)
	type 'a node_t =
		| Empty
		| Node of 'a * 'a zlist_t
	and 'a zlist_t = 'a node_t lazy_t

	(* head *)
	let hd l = match Lazy.force l with
		| Empty -> failwith "hd"
		| Node (h, _) -> h

	(* head *)
	let tl l = match Lazy.force l with
		| Empty -> failwith "tl"
		| Node (_, t) -> t

	(* take that returns a forced list *)
	let rec take_and_collect n l =
		if n <= 0
		then []
		else match Lazy.force l with
			| Empty -> []
			| Node (h, t) -> h::(take_and_collect (n - 1) t)

	(* take that returns a lazy list *)
	let rec take n l = lazy (
		if n <= 0
		then Empty
		else match Lazy.force l with
			| Empty -> Empty
			| Node (h, t) -> Node (h, take (n - 1) t)
	)

	(* skip n elements of the list *)
	let rec skip n l =
		if n <= 0
		then l
		else match Lazy.force l with
			| Empty -> lazy Empty
			| Node (h, t) -> skip (n - 1) t

	(* element at position n *)
	let at n l = match l |> skip n |> take 1 |> Lazy.force with
		| Empty -> None
		| Node (h, _) -> Some h

	(* generate a lazy list using next *)
	let rec from next = lazy (
		match next () with
			| None -> Empty
			| Some s -> Node (s, from next)
	)

	(* generate a lazy list using next, and seed as starting item *)
	let rec from_loop next seed = lazy (
		match seed with
			| None -> Empty
			| Some s -> Node (s, from_loop next (next s))
	)

	(* create a lazy list from a standard one *)
	let rec from_list l = lazy (
		match l with
			| [] -> Empty
			| h::t -> Node (h, from_list t)
	)

	(* return list that results from the application of f to the elements *)
	let rec map f l = lazy (
		match Lazy.force l with
			| Empty -> Empty
			| Node (h, t) -> Node (f h, map f t)
	)

	(* return list that results from the application of f to the elements, in inverse order *)
	let rev_map f l = lazy (
		let rec aux f acc l =
			match Lazy.force l with
				| Empty -> acc
				| Node (h, t) -> aux f (Node (f h, lazy acc)) t in
		aux f Empty l
	)

	(* reverses a lazy list *)
	let reverse l = lazy (
		let rec aux acc l =
			match Lazy.force l with
				| Empty -> acc
				| Node (h, t) -> aux (Node (h, lazy acc)) t in
		aux Empty l
	)

	(* return list with the elements where the predicate is true *)
	let rec filter pred l = match Lazy.force l with
		| Empty -> lazy Empty
		| Node (h, t) ->
			if pred h
			then lazy (Node (h, filter pred t))
			else filter pred t

	(* apply f:'a -> unit to the elements *)
	let rec iter f l = match Lazy.force l with
		| Empty -> ()
		| Node (h, t) ->
			f h;
			iter f t

	(* apply f:'a -> unit to each element and returns that same element *)
	let rec tee f l = lazy (
		match Lazy.force l with
			| Empty -> Empty
			| Node (h, t) ->
				f h;
				(Node (h, tee f t))
	)

	(* transform a lazy list into a concrete list *)
	let rec collect l = match (Lazy.force l) with
		| Empty -> []
		| Node (h, t) -> h::(collect t)
	let (!!) = collect

	(* transform a lazy list into a concrete list, reversed *)
	let rev_collect l = 
		let rec aux acc l = 
			match (Lazy.force l) with
				| Empty -> acc
				| Node (h, t) -> aux (h::acc) t in
		aux [] l
end
