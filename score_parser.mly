%{
	open Types
%}

%token <string> NAME
%token <int> NUMBER
%token <string> DATE
%token GAME_SEPARATOR
%token MAIN_SEPARATOR
%token PLAYER_SEPARATOR
%token LPAREN
%token RPAREN
%token MARKER
%token EOL

%start game
%type <Types.game_result> game

%%

game: 
	| NUMBER GAME_SEPARATOR DATE GAME_SEPARATOR NAME GAME_SEPARATOR scores { GameResult($1, $3, $5, $7) }
	| DATE GAME_SEPARATOR NAME GAME_SEPARATOR scores { GameResult(0, $1, $3, $5) }

scores:
	| score MAIN_SEPARATOR scores { $1::$3 }
	| score EOL { $1::[] }

score:
	| team LPAREN NUMBER RPAREN { Score($1, $3, Winner false) }
	| team LPAREN NUMBER RPAREN MARKER { Score($1, $3, Winner true) }

team:
	| NAME PLAYER_SEPARATOR team { Player $1::$3 }
	| NAME { Player $1::[] }

%%
