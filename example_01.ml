#use "LazyList.ml"

(* open LazyList *)
open Printf

let readline s =
	try Some (read_line ())
	with End_of_file -> None

let input = Z.from readline
let out s = printf "%s\n" s

let () =
	input |> Z.map (fun s -> " -> " ^ s) |> Z.iter out
