open Score_parser
open Sedlex_menhir

let digit = [%sedlex.regexp? '0'..'9']
let twodigits = [%sedlex.regexp? digit, digit]
let fourdigits = [%sedlex.regexp? twodigits, twodigits] 

let rec token lexbuf = 
	let buf = lexbuf.stream in 
	match%sedlex buf with
		| '\t' -> update lexbuf; token lexbuf
		| '(' -> update lexbuf; LPAREN
		| ')' -> update lexbuf; RPAREN
		| ':' -> update lexbuf; GAME_SEPARATOR
		| ';' -> update lexbuf; MAIN_SEPARATOR
		| ',' -> update lexbuf; PLAYER_SEPARATOR
		| '*' -> update lexbuf; MARKER
		| Opt '-', Plus '0'..'9' -> update lexbuf; NUMBER(int_of_string (lexeme lexbuf))
		| fourdigits, '/', twodigits, '/', twodigits -> update lexbuf; DATE(lexeme lexbuf)
		| Star (alphabetic | other_alphabetic | white_space | Chars "\\-_'¿?¡!&0123456789") -> update lexbuf; NAME(lexeme lexbuf)
		| '\n' -> update lexbuf; EOL
		| eof -> update lexbuf; EOL
		| _	-> raise_ParseError lexbuf
