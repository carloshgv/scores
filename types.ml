type
	game_result = GameResult of id * date * string * score list and
	id = int and
	date = string and
	points = int and
	score = Score of player list * points * winner and
	player = Player of string and
	winner = Winner of bool

type player_summary = player * int * int
type game_count = string * int
type player_count = string * int

type exn += Record_not_found of int

module type FORMATTER = sig
	val format :
		[
			| `Result of game_result
			| `Results of game_result list
			| `Player_summaries of player_summary list
			| `Message_int of string * int
			| `Message of string * string
			| `Error of string * string
			| `List_of_strings of string list
			| `Game_counts of game_count list
			| `Player_counts of player_count list
		] -> string
end

module type SCORES = sig
	val read_score_list : string -> game_result list
	val save_score_list : string -> game_result list -> unit
	val count_games : scores:game_result list ->  int
	val count_game : scores:game_result list -> string -> int
	val count_all : scores:game_result list -> int
	val create : scores:game_result list -> string -> game_result list
	val update : scores:game_result list -> int -> string -> game_result list
	val delete : scores:game_result list -> int -> game_result list
	val list_players : scores:game_result list -> player_summary list
	val wins : player -> scores:game_result list -> game_count list
	val loses : player -> scores:game_result list -> game_count list
	val winners : string -> scores:game_result list -> player_count list
	val losers : string -> scores:game_result list -> player_count list
end

module type COMMANDER = sig
	type command
	val run : game_result list -> command -> game_result list
end
