open Score_lexer
open Score_parser
open Printf
open String
open Types

(* command reader *)
let read_command () =
	try (
		match (read_line ()) with
		| "exit" -> None
		| comm -> Some (Str.split (Str.regexp " ") comm)
	)
	with End_of_file -> None

(* input decorators *)
let interactive f =
	print_string ">> ";
	f ()
let listener f = f ()

(* main loop *)
let rec mainloop ~scores:input ~mode (module Commander:COMMANDER with type command = string list) =
	match mode read_command with
		| Some command ->
			let result = Commander.run input command in
			mainloop ~scores:result ~mode (module Commander)
		| None -> ()

let () =
	let comm_console = (module Commander.MakeSimple(Formatters.Console) : COMMANDER with type command = string list) in
	let comm_json = (module Commander.MakeSimple(Formatters.JSON) : COMMANDER with type command = string list) in
	let (mode, comm) = if Array.length Sys.argv == 1 then (interactive, comm_console) else (listener, comm_json) in
	mainloop ~scores:[] ~mode:mode comm
