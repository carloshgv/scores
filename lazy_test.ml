#use "lazylist.ml";;
open Z;;
open Printf;;

let fake_db_access () = from_loop (fun n -> printf "--- read row %d\n" n; Some (n+1)) (Some 1);;

let transform x = printf "+++ performing transformation %d -> %d\n" x (x*2); x*2;;

let test x = printf "*** checking if %d mod 3 == 0 (%B)\n" x (x mod 3 == 0); x mod 3 == 0;;

let rec seq ~first ~last =
	printf "--- create row %d\n" first;
	if first > last
	then []
	else
		first::(seq (first+1) last);;
