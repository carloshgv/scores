open List
open Types
open Printf
open String
open LazyList
open Sedlex_menhir

module StrSet = Set.Make(String)

module Scores : SCORES = struct
	let same_id id (GameResult (gid, _, _, _)) = id == gid
	let different_id id (GameResult (gid, _, _, _)) = id != gid
	let lex s =
		let lexbuf = create_lexbuf (Sedlexing.Utf8.from_string s) in
		sedlex_with_menhir Score_lexer.token Score_parser.game lexbuf

	(* read input file *)
	let read_score_list filename =
		let file = open_in filename in
		let readline () =
			try Some ((input_line file))
			with End_of_file -> None in
		let list = Z.from readline |> Z.rev_map lex |> Z.rev_collect in close_in file; list

	let save_score_list filename scores =
		let sorted = scores |> List.sort (fun (GameResult (_, date1, _, _)) (GameResult (_, date2, _, _)) -> String.compare date2 date1) in
		let out = open_out filename in
		sorted |> List.iteri (fun i (GameResult (_, date, game, scores)) -> fprintf out "%s" (Formatters.scores_file (`Result (GameResult (i+1, date, game, scores))))); close_out out

	let count_games ~scores:input =
		let set = input |> fold_left (fun set (GameResult (_, _, game, _)) -> StrSet.add game set) StrSet.empty in
		StrSet.cardinal set

	let count_game ~scores:input game_name =
		input |> filter (fun (GameResult (_, _, game, _)) -> lowercase game = lowercase game_name) |> List.length

	let count_all ~scores:input = List.length input

	let create ~scores:input line =
		let input_data = lex line in
		let new_result = match input_data with
			| GameResult (0, a, b, c) -> GameResult (List.length input+1, a, b , c)
			| g -> g in
		new_result::input

	let update ~scores:input update_id line =
		let filtered = input |> filter (different_id update_id) in
			if List.length filtered == List.length input
			then raise (Record_not_found update_id)
			else
				let GameResult (_, new_date, new_name, new_scores) = lex line in
				let updated = GameResult (update_id, new_date, new_name, new_scores) in
				let output = updated::filtered in
				output

	let delete ~scores:input delete_id =
		let output = input |> filter (different_id delete_id) in
		output

	let list_players ~scores =
		let count = Hashtbl.create 117 in
		let scores_list = scores |> List.map (fun (GameResult (_, _, _, s)) -> s) |> flatten in
		let init p = if not (Hashtbl.mem count p) then Hashtbl.add count p (0, 0) else () in
		let add_win = (function Player p -> let (w, l) = init p; Hashtbl.find count p in Hashtbl.replace count p (w+1, l)) in
		let add_lose = (function Player p -> let (w, l) = init p; Hashtbl.find count p in Hashtbl.replace count p (w, l+1)) in
		scores_list |> List.iter (fun (Score (players, _, Winner win)) -> match win with
			| true -> players |> List.iter add_win
			| false -> players |> List.iter add_lose
		);
		Hashtbl.fold (fun k (w, l) acc -> (Player k, w, l)::acc) count []

	let wins_loses player ~scores = 
		let count_wins = Hashtbl.create 117 in
		let count_loses = Hashtbl.create 117 in
		let scores_list = scores |> 
			List.map (fun (GameResult (_, _, game, scores)) -> List.map (fun score -> (game, score)) scores) |> 
			flatten in
		let add_to table game =
			if not (Hashtbl.mem table game) then Hashtbl.add table game 0 else ();
			let current = Hashtbl.find table game in Hashtbl.replace table game (current + 1) in

		scores_list |> List.iter (fun (game, (Score (players, _, Winner win))) -> match win with
			| true -> if List.mem player players then add_to count_wins game else ()
			| false -> if List.mem player players then add_to count_loses game else ()
		);
		let fold table = Hashtbl.fold (fun k w acc -> (k, w)::acc) table [] in
		(fold count_wins, fold count_loses)

	let wins player ~scores = 
		let (wins, _) = wins_loses player scores in sort (fun (_, a) (_, b) -> b - a) wins

	let loses player ~scores =
		let (_, loses) = wins_loses player scores in sort (fun (_, a) (_, b) -> b - a) loses

	let winners_losers selectedGame ~scores = 
		let count_winners = Hashtbl.create 117 in
		let count_losers = Hashtbl.create 117 in
		let scores_list = scores |> List.filter (fun (GameResult(_, _, game, _)) -> String.compare game selectedGame == 0) |>
			List.map (fun (GameResult (_, _, game, scores)) -> List.map (fun score -> (game, score)) scores) |> 
			flatten in
		let add_to table player =
			if not (Hashtbl.mem table player) then Hashtbl.add table player 0 else ();
			let current = Hashtbl.find table player in Hashtbl.replace table player (current + 1) in

		scores_list |> List.iter (fun (game, (Score (players, _, Winner win))) -> match win with
			| true -> players |> List.iter (fun (Player p) -> add_to count_winners p)
			| false -> players |> List.iter (fun (Player p) -> add_to count_losers p)
		);
		let fold table = Hashtbl.fold (fun k w acc -> (k, w)::acc) table [] in
		(fold count_winners, fold count_losers)

	let winners game ~scores = 
		let (winners, _) = winners_losers game scores in sort (fun (_, a) (_, b) -> b - a) winners

	let losers game ~scores =
		let (_, losers) = winners_losers game scores in sort (fun (_, a) (_, b) -> b - a) losers		
end

module MakeSimple(F: FORMATTER) : (COMMANDER with type command = string list) = struct
	type command = string list

	let print_format element = print_endline (F.format element)
	let run input = function
		| "list"::"games"::_ ->
			let set = input |> fold_left (fun set (GameResult (_, _, game, _)) -> StrSet.add game set) StrSet.empty in
			print_format (`List_of_strings (StrSet.elements set));
			input
		| "list"::"players"::_ ->
			print_format (`Player_summaries (Scores.list_players input)); input
		| "list"::_ -> print_format (`Results input); input

		| "count"::[] -> print_format (`Message_int ("number of results", (Scores.count_all input))); input
		| "count"::"games"::_ -> print_format (`Message_int ("number of games", (Scores.count_games input))); input
		| "count"::game ->
			let game_name = concat " " game in
			print_format (`Message_int ("occurrences of " ^ game_name, Scores.count_game input game_name)); input

		| "wins"::player ->
			let player_name = concat " " player in
			print_format (`Game_counts (Scores.wins (Player player_name) input));
			input

		| "loses"::player ->
			let player_name = concat " " player in
			print_format (`Game_counts (Scores.loses (Player player_name) input));
			input

		| "winners"::game ->
			let game_name = concat " " game in
			print_format (`Player_counts (Scores.winners game_name input));
			input

		| "losers"::game ->
			let game_name = concat " " game in
			print_format (`Player_counts (Scores.losers game_name input));
			input

		| "create"::[] -> input
		| "create"::line -> (
			try
				let output = Scores.create ~scores:input (concat " " line) in
				print_format (`Result (hd output));
				output
			with _ -> print_format (`Error ("wrong record", (concat " " line))); input
		)

		| "update"::[] -> input
		| "update"::id::line -> (
			try
				let output = Scores.update ~scores:input (int_of_string id) (concat " " line) in
				print_format (`Result (hd output));
				output
			with
				| Record_not_found i -> print_format (`Error ("record not found", string_of_int i)); input
				| _ -> print_format (`Error ("wrong record", (concat " " line))); input
		)

		| "delete"::[] -> input
		| "delete"::id::_ ->
			let output = Scores.delete ~scores:input (int_of_string id) in
			print_format (`Message_int ("deleted records", List.length input - List.length output));
			output

		| "load"::[] -> input
		| "load"::filename::_ -> (
			try
				let new_scores = Scores.read_score_list filename in
				print_format (`Message ("loaded", filename));
				new_scores
			with
				| Sys_error e -> print_format (`Error ("error reading file", e)); input
				| ParseError e -> print_format (`Error ("wrong format", string_of_ParseError e)); input
				| _ as e -> print_format (`Error ("unknown error", Printexc.to_string e)); input
			)

		| "save"::[] -> input
		| "save"::filename::_ -> (
			try
				Scores.save_score_list filename input;
				print_format (`Message ("saved", filename));
				input
			with Sys_error e -> print_format (`Error ("couldn't write file", e)); input
		)

		| "new"::_ -> []
		| other::_ -> print_format (` Error ("command not found", other)); input
		| [] -> input
end
